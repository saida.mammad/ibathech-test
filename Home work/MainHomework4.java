import java.util.Arrays;

public class MainHomework4 {
    public static void main(String[] args) {
        Pet pet = new Pet();

        pet.setSpecies("dog");
        pet.setNickname("Rock");
        pet.setAge(5);
        pet.setTrickLevel(75);
        pet.setHabits(new String[]{"eat", "drink", "sleep"});

        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println();

        Human human = new Human();

        human.setName("Michael");
        human.setSurname("Karleone");
        human.setYear(1977);
        human.setIq(90);

        human.greetPet(pet);
        human.describePet(pet);

        Human mother = new Human();

        mother.setName("Jane");
        mother.setSurname("Karleone");

        Human father = new Human();

        father.setName("Vito");
        father.setSurname("Karleone");

        System.out.println();

        System.out.println(human.toString(mother, father, pet));
        System.out.println(pet.toString());
    }
}