import java.util.Random;
import java.util.Scanner;

public class Homework2 {

    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        System.out.println("You have to choose direction to shoot from this table");
        char[][] shootingField = new char[6][6];
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 1; i < 6; i++) {
            System.out.print(i + " |");
            for (int j = 0; j < 5; j++) {

                System.out.print(shootingField[i][j] + " - |");
            }
            System.out.println();
        }
        Random random = new Random();
        int row = random.nextInt(5);
        int column = random.nextInt(5);

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter row coordinate. It can be any number from 1 to 5 : ");
        while (true) {
            if (!input.hasNextInt()) {
                System.out.println("This input is not a number");
                input.next();
            }
            int playerRow = input.nextInt();
            while (true) {
                if (playerRow <= 0 || playerRow >= 5) {
                    System.out.println("Entered number isn't in the range of playing field. It have to be from 1 to 5");
                    System.out.println("Please enter row coordinate. It can be any number from 1 to 5 :");
                    playerRow = input.nextInt();
                } else {
                    break;
                }
            }
            System.out.println("Please enter column coordinate. It can be any number from 1 to 5");
            int playerColumn;
            while (true) {
                if (!input.hasNextInt()) {
                    System.out.println("This input is not a number");
                    input.next();
                }
                playerColumn = input.nextInt();

                if (playerColumn <= 0 || playerColumn >= 5) {
                    System.out.println("Entered number isn't in the range of playing field. It have to be from 1 to 5");
                    System.out.println("Please enter column coordinate:");
                } else {
                    break;
                }
            }
            if (row == playerRow - 1 && column == playerColumn - 1) {
                System.out.println("Congratulations! You have won!");
                shootingField[row][column] = 'X';
                System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
                for (int i = 1; i < 6; i++) {
                    System.out.print(i + "| - ");
                    for (int j = 0; j < 5; j++) {
                        System.out.print(shootingField[row][column] + " | ");
                    }
                    System.out.println();
                }
                break;
            } else {
                System.out.println("Your coordinates are wrong!");
                shootingField[playerRow - 1][playerColumn - 1] = '*';
                System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
                for (int i = 1; i < 6; i++) {
                    System.out.print(i + " | -");
                    for (int j = 0; j < 5; j++) {
                        System.out.print(" " + shootingField[row][column] + " | ");
                    }

                    System.out.println();
                }
                System.out.println("Please enter row coordinate. It can be any number from 1 to 5 :");
            }
        }
    }
}


